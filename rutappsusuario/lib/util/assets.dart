Map Assets = {

  //LOGIN
  "loginLogo" : 'assets/images/login/logo.png',
  "loginBg" : 'assets/images/login/bg.png',
  "loginGnt01" : 'assets/images/login/gente01.png',
  "loginGnt02" : 'assets/images/login/gente02.png',
  //HOME
  "homeLogo" : 'assets/images/home/logo.png',
  "homeOut" : 'assets/images/home/goOut.png',
  "homeNtf" : 'assets/images/home/notifications.png',
  //BACKGROUNDS
  "homeBgBlue" : 'assets/images/home/bgAzul.png'
};