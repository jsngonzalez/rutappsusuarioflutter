import 'package:flutter/material.dart';

class Triangulo extends StatelessWidget{



  Triangulo({
    this.margenes : const EdgeInsets.only(top: 0, left: 0, bottom: 0, right: 0 ),
    this.ancho : 20,
    this.alto : 20,
    this.fondo : Colors.white
  });

  final EdgeInsets margenes;
  final double ancho;
  final double alto;
  final Color fondo;



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: EdgeInsets.only(
          top: margenes.top,
          left: margenes.left,
          bottom: margenes.bottom,
          right: margenes.right
      ),
      width: 1,
      height:1,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.black12,
              blurRadius: 10,
              offset: Offset(0, -4),
              spreadRadius: 10
          )
        ],
        border: Border(
          left: BorderSide(
              width: ancho,
              color: Colors.transparent,
              style: BorderStyle.solid
          ),
          right: BorderSide(
              width: ancho,
              color: Colors.transparent,
              style: BorderStyle.solid
          ),
          bottom: BorderSide(
              width: alto,
              color: fondo,
              style: BorderStyle.solid
          ),
        ),
      ),
    );
  }
}