import 'package:flutter/material.dart';
import 'package:rutappsusuario/widgets/login/btnLogin.dart';
import 'package:rutappsusuario/widgets/login/dots.dart';
import 'package:rutappsusuario/widgets/login/page1.dart';
import 'package:rutappsusuario/widgets/login/page2.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:rutappsusuario/util/assets.dart';

import 'package:rutappsusuario/services/auth.dart';

class _LoginMainPageState extends State<LoginMainPage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        body: new LoginPanel()
    );
  }
}

class LoginMainPage extends StatefulWidget {
  LoginMainPage({Key key}) : super(key: key);

  @override
  _LoginMainPageState createState() => new _LoginMainPageState();
}

class _LoginPaneState extends State<LoginPanel> {
  final _controller = new PageController();
  final List<Widget> _pages = [
    Page1(),
    Page2(),
    //Page3(),
  ];
  int page = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new SafeArea(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            width: MediaQuery.of(context).size.width * 0.5,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: new Image(
              image: AssetImage(Assets["loginLogo"]),
              fit: BoxFit.fitWidth,
            ),
          ),

        new Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(bottom: 150),
              child:new Image(
                image: AssetImage(Assets["loginBg"]),
                fit: BoxFit.fitWidth,
              ),
            ),
            new Positioned.fill(
                child: new PageView.builder(
                  physics: new AlwaysScrollableScrollPhysics(),
                  controller: _controller,
                  itemCount: _pages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _pages[index % _pages.length];
                  },
                  onPageChanged: (int p) {
                    setState(() {
                      page = p;
                    });
                  },
                ),
            ),
          ],
        ),
        //new Padding(padding: EdgeInsets.only(bottom: 150.0)),
          new Padding(
            padding: const EdgeInsets.all(1.0),
            child: new DotsIndicator(
              controller: _controller,
              itemCount: _pages.length,
              onPageSelected: (int page) {
                _controller.animateToPage(
                  page,
                  duration: const Duration(milliseconds: 4800),
                  curve: Curves.ease,
                );
              },
            ),
          ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new LoginButton(
                btnColor: Colors.blue,
                icono: MdiIcons.facebookBox,
                texto: "Inicia sesión",
                onClick: (int a) {
                  fbLogin(context);
                }),
            new LoginButton(
                btnColor: Colors.red,
                icono: MdiIcons.googlePlusBox,
                texto: "Inicia sesión",
                onClick: (int a) {
                  googleLogin(context);
                }),
          ],
        )
      ],
    ));
  }
}

class LoginPanel extends StatefulWidget {
  LoginPanel({Key key}) : super(key: key);

  @override
  _LoginPaneState createState() => new _LoginPaneState();
}