import 'package:flutter/material.dart';


import 'package:rutappsusuario/widgets/bienvenida/app_bar.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';

import 'package:rutappsusuario/views/bienvenida.dart';

import 'package:flare_flutter/flare_actor.dart';

const String ANIM_A = "reposo";
const String ANIM_B = "buscando";

class _HomePanelState extends State<HomePanel> {

  String currentAnimation = ANIM_A;
  bool isAnimating = false;

  Map<String,dynamic> usuario = new Map();

  @override
  initState(){
    super.initState();
    _validUserData();
  }

  _validUserData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      var strusuario = (prefs.getString('user') ?? '');

      print("********HOLA**************");
      usuario = jsonDecode(strusuario);

      if(usuario["nombre"] != null && usuario["nombre"] != ""){
        usuario["primerNombre"] = usuario["nombre"].split(" ")[0];
      }else{
        usuario["primerNombre"] = "asd";
        Navigator.pop(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.white,
        body: new Scaffold(
          appBar: new HomeAppBar(),
          body: new BienvenidaPanel(user: usuario)
          /*
          body: new Column(
            children: <Widget>[
              Container(
                color: Colors.red,
                height: 586,
                width: 1000,
                child: FlareActor(
                  "assets/anims/zally.flr",
                  alignment: Alignment.center,
                  fit: BoxFit.cover,
                  animation: currentAnimation,
                  callback: (string) {
                    isAnimating = false;
                    debugPrint(string);
                  },
                )
              ),
              Expanded(
                child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        RaisedButton(
                            color: Colors.blueAccent,
                            child: Text(
                              "Sin tìtulo",
                              style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () {
                              _animateTo(ANIM_A);
                            }),
                        RaisedButton(
                            color: Colors.black,
                            child: Text(
                              "Sarcasmo",
                              style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () {
                              _animateTo(ANIM_B);
                            }),
                      ],
                    )),
              )
            ],
          ),
          */
        )
    );
  }



  void _animateTo(String anim) {
    setState(() {
      currentAnimation = anim;
    });
    /*
    if (_canAnimateTo(anim)) {
      isAnimating = true;
      setState(() {
        currentAnimation = anim;
      });
    }
    */
  }

  bool _canAnimateTo(String anim) {
    return !isAnimating;
    /*
    if (currentAnimation == ANIM_JUST_NIGHT) {
      return anim == ANIM_NIGHT_TO_DAY;
    }
    return (!isAnimating && currentAnimation != anim);
    */
  }
}

class HomePanel extends StatefulWidget {
  HomePanel({Key key}) : super(key: key);

  @override
  _HomePanelState createState() => new _HomePanelState();
}