import 'package:flutter/material.dart';

class DialogError {

  Future<dynamic> neverSatisfied(BuildContext context,String texto) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (context) {
        return AlertDialog(
          title: Text('¡Lo sentimos!',textAlign: TextAlign.center,),
          content: SingleChildScrollView(
              child: new Center(
                child: new Text(texto,textAlign: TextAlign.center,),
              )
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar',style: new TextStyle(color: Colors.white),),
              color: Colors.blue,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
