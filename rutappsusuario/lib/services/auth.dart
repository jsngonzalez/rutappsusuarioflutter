import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:rutappsusuario/util/service_util.dart';

import 'package:rutappsusuario/models/rutapps_user.dart';

import 'package:rutappsusuario/widgets/DialogError.dart';

import 'package:shared_preferences/shared_preferences.dart';


final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

ServiceUtil _api = new ServiceUtil();
DialogError _alert = new DialogError();



Future<FirebaseUser> _handleSignIn() async {
  GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  GoogleSignInAuthentication googleAuth = await googleUser.authentication;
  FirebaseUser user = await _auth.signInWithGoogle(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  print("signed in " + user.displayName);
  return user;
}

void fbLogin(BuildContext context){
  _alert.neverSatisfied(context, "Por el momento no se encuentra áctiva esta funcionalidad");
}

void googleLogin(BuildContext context){
  _handleSignIn()
    .then((FirebaseUser user){

      Map usuario = new Map();
      usuario["uid"] = user.uid;
      usuario["correo"] = user.email;
      usuario["foto"] = user.photoUrl;
      usuario["nombre"] = user.displayName;

      LoginOnBD(context,usuario);

    })
    .catchError((e) {
      print("error");
      print(e);
    });
}

void LoginOnBD(BuildContext context,Map usuario){
  _api.login(usuario)
      .then((dynamic res){
        if(res["error"].toString() == "0") {
          SaveInPref(usuario);
          Navigator.pushNamed(context, '/home');
        }else{
          _alert.neverSatisfied(context,res["response"]);
        }
      });
}

SaveInPref(Map usuario) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String strUser = jsonEncode(usuario);
  prefs.setString("user", strUser);
}