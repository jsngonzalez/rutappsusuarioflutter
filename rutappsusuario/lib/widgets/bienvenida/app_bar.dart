import 'package:flutter/material.dart';
import 'package:rutappsusuario/util/assets.dart';

class HomeAppBar extends PreferredSize{

  @override
  Size get preferredSize {
    return new Size.fromHeight(55.0);
  }


  @override
  Widget build(BuildContext context) {
    return new AppBar(
      titleSpacing: 5.0,
      centerTitle: true,
      title: Container(
        //padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0),
        width: MediaQuery.of(context).size.width * 0.4,
        alignment: FractionalOffset.centerRight,
        child: new Image(
          image: AssetImage(Assets["homeLogo"]),
          fit: BoxFit.fitWidth,
        ),
      ),
      actions: <Widget>[
        new Image(
          image: AssetImage(Assets["homeNtf"]),
          fit: BoxFit.fitWidth,
        )
      ],
      leading:  Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: new Image(
              image: AssetImage(Assets["homeOut"]),
              fit: BoxFit.fill,

            ),
            onPressed: () { Navigator.pop(context); },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      backgroundColor: Colors.white,
      elevation: 1,
    );
  }
}