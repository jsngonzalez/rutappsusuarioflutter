import 'package:rutappsusuario/util/netwwork_util.dart';
import 'package:rutappsusuario/models/rutapps_user.dart';

const SRV = {
  "protocol":"https://",
  "domain":"api-zironet.c9users.io/",
  "module":"cliente/",
  "version":"v1/"
};

class ServiceUtil{
  NetworkUtil _rest = new NetworkUtil();


  Future<dynamic> login(Map usuario){

    const metodo = "login.json";
    var FINAL_URL = SRV["protocol"]+SRV["domain"]+SRV["module"]+SRV["version"]+metodo;

    Map<String, String> jsonData = {"nombre":usuario["nombre"],"foto":usuario["foto"],"uid":usuario["uid"],"correo":usuario["correo"]};

    Map<String, String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };

    return _rest.post(FINAL_URL,body:jsonData,headers:headers).then((dynamic res){return res;});
  }
}


