import 'package:flutter/material.dart';
import 'package:rutappsusuario/views/login.dart';
import 'package:rutappsusuario/views/home.dart';

final routes = {
  '/': (BuildContext context) => new LoginMainPage(),
  '/login': (BuildContext context) => new LoginMainPage(),
  '/home': (BuildContext context) => new HomePanel()
};