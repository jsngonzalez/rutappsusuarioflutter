import 'package:flutter/material.dart';
import 'package:rutappsusuario/util/assets.dart';

class HomeBg extends StatelessWidget{

  HomeBg({
    this.Imagen
  });

  final String Imagen;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      decoration: new BoxDecoration(
          image: DecorationImage(
              image: new AssetImage(Assets[Imagen]),
              fit: BoxFit.fitWidth
          )
      ),
    );
  }
}