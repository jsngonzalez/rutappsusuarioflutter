import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;



class NetworkUtil{
  static NetworkUtil _instance = NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url){
    return http.get(url)
      .then((http.Response res){
          try {
            return _workAround(res);
          }catch(e){
            return {"error":1,"response":"No ha sido posible conectarse con el servidor","data":e.toString()};
          }
      });
  }

  Future<dynamic> post(String url,{Map headers, body, encoding}){
    Map<String, Map<String, String>> data = {"data":body};
    body = jsonEncode(data);
    return http.post(url,body: body,headers: headers,encoding: encoding)
        .then((http.Response res){
          try {
            return _workAround(res);
          }catch(e){
            return {"error":1,"response":"No ha sido posible conectarse con el servidor","data":e.toString()};
          }
        });
  }

  Map<String,dynamic> _workAround(http.Response res){
    final String _res = res.body;
    final int _statusCode = res.statusCode;

    if(_statusCode < 200 || _statusCode > 400 || json == null){
      return {"error":"0","response":"Por favor verifica tu conexion a internet","data":_res};
    }
    return jsonDecode(_res);
  }
}