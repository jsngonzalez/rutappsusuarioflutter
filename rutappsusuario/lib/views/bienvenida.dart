import 'package:flutter/material.dart';

import 'package:rutappsusuario/widgets/bienvenida/home_bg.dart';
import 'package:rutappsusuario/widgets/bienvenida/triangulo.dart';

import 'package:flare_flutter/flare_actor.dart';

class _BienvenidaPanelState extends State<BienvenidaPanel>{

  bool isAnimating = false;

  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new HomeBg(Imagen: "homeBgBlue"),
        new Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                  decoration: new BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 6,
                            offset: Offset(0, 5),
                            spreadRadius: 5
                        )
                      ]
                  ),
                  width: MediaQuery.of(context).size.width * 0.8,
                  padding: EdgeInsets.all(10),
                  child:new Text(
                    '¡Bienvenido a Rutapps, ${widget.user["primerNombre"]}! La forma más sencilla de controlar tus servicios o rutas contratadas. Yo soy tu asistente Zally. Empecemos agregando un nuevo servicio.',
                    style: new TextStyle(
                        letterSpacing: 0.1,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        height: 1.1
                    ),
                  )
              ),
              new Triangulo(
                  margenes: EdgeInsets.only(top: 19),
                  ancho: 15,
              ),
              new Container(
                  height: MediaQuery.of(context).size.width * 0.4,
                  width: MediaQuery.of(context).size.width * 0.5,
                  //color: Colors.red,
                  alignment: FractionalOffset.topLeft,
                  padding: new EdgeInsets.only(bottom: 0,right: 0),
                  child: FlareActor(
                    "assets/anims/zally.flr",
                    alignment: Alignment.topLeft,
                    fit: BoxFit.contain,
                    animation: "reposo",
                    callback: (string) {
                      isAnimating=false;
                      debugPrint(string);
                    },
                  )
              ),
              new Padding(padding: EdgeInsets.only(bottom: 30))
            ],
          ),
        ),
      ],
    );
  }
}

class BienvenidaPanel extends StatefulWidget{
  BienvenidaPanel({Key key,this.user:const {"primerNombre":"Usuario"}}) : super(key: key);
  final Map<String,dynamic> user ;


  @override
  _BienvenidaPanelState createState() => new _BienvenidaPanelState();
}