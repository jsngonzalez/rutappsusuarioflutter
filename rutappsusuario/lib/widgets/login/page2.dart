import 'package:flutter/material.dart';
import 'package:rutappsusuario/util/assets.dart';

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return new Stack(
      alignment: FractionalOffset.bottomCenter,
      children: [
        new Positioned(
          child: new Container(
            transform: Matrix4.translationValues(0.0, -110.0, 0.0),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child:new Image(
              image: AssetImage(Assets["loginGnt02"]),
              fit: BoxFit.fitWidth,
            ),
          ),
          width: MediaQuery.of(context).size.width * 0.6,
        ),
        new Container(
          width: MediaQuery.of(context).size.width * 0.7,
          child:new Text(
            "Gestiona tus servicios de rutas fácilmente desde tu teléfono",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Color.fromRGBO(41, 41, 41, 1),
              letterSpacing: 0.8,


            ),
          ),
        )
      ],
    );
  }
}